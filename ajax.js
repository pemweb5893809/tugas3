const button = document.querySelector("#submit");
button.addEventListener("click", () => {
  const nama = document.getElementById("nama").value;
  const nim = document.getElementById("nim").value;
  const program_studi = document.getElementById("program_studi").value;
  const jenis_kelamin = document.getElementById("jenis_kelamin").value;
  const url = "ajax.php?nama=" + encodeURIComponent(nama) + "&nim=" + encodeURIComponent(nim) + "&jenis_kelamin=" + encodeURIComponent(jenis_kelamin) + "&program_studi=" + encodeURIComponent(program_studi);
  const resultDiv = document.getElementById("result");
  axios.get(url).then((response) => {
    alert(response.data);
  });
});
